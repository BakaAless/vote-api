package fr.debatecontest.voteapi.exceptions;

public class CandidateNotFoundException extends RuntimeException {

    public CandidateNotFoundException(Long id) {
        super("Could not find candidate " + id);
    }

}
