package fr.debatecontest.voteapi.controllers;

import fr.debatecontest.voteapi.entities.Candidate;
import fr.debatecontest.voteapi.entities.Session;
import fr.debatecontest.voteapi.exceptions.CandidateNotFoundException;
import fr.debatecontest.voteapi.repositories.CandidateRepository;
import fr.debatecontest.voteapi.requests.Connection;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CandidateController extends ConnectionController {

    private final CandidateRepository repository;

    public CandidateController(CandidateRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/candidates")
    public List<Candidate> all() {
        return repository.findAll();
    }

    @PostMapping("/candidates")
    public ResponseEntity<Candidate> newCandidate(@RequestBody Connection connection, @RequestBody Candidate newCandidate) {
        if (!this.connect(connection)) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(repository.save(newCandidate));
    }

    @PostMapping("/create_candidate/{name}/{lastName}")
    @ResponseBody
    public ResponseEntity<Candidate> newCandidate(@RequestBody Connection connection, @PathVariable String name, @PathVariable String lastName) {
        if (!this.connect(connection)) {
            return ResponseEntity.badRequest().build();
        }
        Candidate candidate = new Candidate();
        candidate.setFirstName(name);
        candidate.setLastName(lastName);
        return ResponseEntity.ok(repository.save(candidate));
    }

    @PostMapping("/candidates/{id}")
    public ResponseEntity<Candidate> one(@PathVariable Long id) {
        return repository.findById(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

}
