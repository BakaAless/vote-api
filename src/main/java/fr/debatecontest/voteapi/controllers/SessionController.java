package fr.debatecontest.voteapi.controllers;

import fr.debatecontest.voteapi.entities.Candidate;
import fr.debatecontest.voteapi.entities.Session;
import fr.debatecontest.voteapi.entities.Vote;
import fr.debatecontest.voteapi.repositories.CandidateRepository;
import fr.debatecontest.voteapi.repositories.SessionRepository;
import fr.debatecontest.voteapi.requests.Connection;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class SessionController extends ConnectionController {

    private final SessionRepository repository;
    private final CandidateRepository candidateRepository;

    public SessionController(SessionRepository repository, CandidateRepository candidateRepository) {
        this.repository = repository;
        this.candidateRepository = candidateRepository;
    }

    @GetMapping("/sessions")
    public List<Session> all() {
        return repository.findAll();
    }

    @PostMapping("/create_session")
    @ResponseBody
    public ResponseEntity<Session> newSession(@RequestBody Connection connection) {
        if (!this.connect(connection)) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(repository.save(new Session()));
    }

    @GetMapping("/sessions/{id}")
    public ResponseEntity<Session> one(@PathVariable Long id) {
        Optional<Session> session = repository.findById(id);
        return session.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/create_vote/{id}")
    public ResponseEntity<Vote> createVote(@RequestBody Connection connection, @PathVariable Long id) {
        if (!this.connect(connection)) {
            return ResponseEntity.badRequest().build();
        }

        Vote vote = new Vote();
        repository.flush();
        if (repository.findById(id)
                .map(session -> {
                    session.getVotes().add(vote);
                    vote.setSessionId(session.getId());
                    return repository.save(session);
                }).isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(vote);
    }

    @PostMapping("/add_candidate/{idSession}/{idCandidate}")
    public ResponseEntity<HttpStatus> addCandidate(@RequestBody Connection connection, @PathVariable Long idSession, @PathVariable Long idCandidate) {
        if (!this.connect(connection)) {
            return ResponseEntity.ok(HttpStatus.UNAUTHORIZED);
        }

        repository.flush();
        candidateRepository.flush();
        try {
            Session session = this.one(idSession).getBody();
            Candidate candidate = this.candidateRepository.findById(idCandidate).get();

            if (candidate.getSessionsId().contains(session.getId()) || session.getCandidates().contains(candidate))
                return ResponseEntity.ok(HttpStatus.CONFLICT);

            session.getCandidates().add(candidate);
            candidate.getSessionsId().add(session.getId());

            repository.save(session);
            candidateRepository.save(candidate);
        } catch (Exception e) {
            return ResponseEntity.ok(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }

    @GetMapping("/sessions/{id}/candidates")
    public List<Candidate> candidates(@PathVariable Long id) {
        return this.one(id).getBody().getCandidates();
    }

}
