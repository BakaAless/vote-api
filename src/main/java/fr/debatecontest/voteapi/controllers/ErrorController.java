package fr.debatecontest.voteapi.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ErrorController {

    @GetMapping("/error")
    @PostMapping("/error")
    @DeleteMapping("/error")
    public HttpStatus error() {
        return HttpStatus.BAD_REQUEST;
    }

}
