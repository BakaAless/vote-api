package fr.debatecontest.voteapi.controllers;

import fr.debatecontest.voteapi.configuration.Config;
import fr.debatecontest.voteapi.requests.Connection;
import jakarta.inject.Inject;

import java.util.ArrayList;

public abstract class ConnectionController {

    @Inject
    protected Config config;

    protected boolean connect(Connection connection) {
        return this.config.get("authorized-tokens", new ArrayList<>()).contains(connection.getToken());
    }

}
