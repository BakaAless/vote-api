package fr.debatecontest.voteapi.controllers;

import fr.debatecontest.voteapi.requests.Connection;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class SettingsController extends ConnectionController {

    @PostMapping("/settings/add_token/{token}")
    public HttpStatus addToken(@RequestBody Connection connection, @PathVariable String token) {
        if (!this.connect(connection)) {
            return HttpStatus.UNAUTHORIZED;
        }
        List<String> tokens = this.config.get("authorized-tokens", new ArrayList<>());
        tokens.add(token);
        this.config.write("authorized-tokens", tokens);
        return HttpStatus.OK;
    }

    @PostMapping("/settings/remove_token/{token}")
    public HttpStatus removeToken(@RequestBody Connection connection, @PathVariable String token) {
        if (!this.connect(connection)) {
            return HttpStatus.UNAUTHORIZED;
        }
        List<String> tokens = this.config.get("authorized-tokens", new ArrayList<>());
        tokens.remove(token);
        this.config.write("authorized-tokens", tokens);
        try {
            this.config.save();
        } catch (IOException e) {
            e.printStackTrace();
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }

}
