package fr.debatecontest.voteapi.requests;

public class Connection {

    private String token;

    public Connection() {
    }

    public Connection(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
