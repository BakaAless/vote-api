package fr.debatecontest.voteapi.repositories;

import fr.debatecontest.voteapi.entities.Session;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SessionRepository extends JpaRepository<Session, Long> {
}
