package fr.debatecontest.voteapi.repositories;

import fr.debatecontest.voteapi.entities.Candidate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CandidateRepository extends JpaRepository<Candidate, Long> {
}
