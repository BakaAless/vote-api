package fr.debatecontest.voteapi.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Candidate {

    @Id
    @GeneratedValue
    private Long id;
    private String firstName;
    private String lastName;
    @ElementCollection
    @Cascade(CascadeType.ALL)
    private List<Long> sessionsId = new ArrayList<>();

    public Candidate() {
    }

    public Candidate(Long id, String firstName, String lastName, List<Long> sessionsId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.sessionsId = sessionsId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setSessionsId(List<Long> sessionsId) {
        this.sessionsId = sessionsId;
    }

    public List<Long> getSessionsId() {
        return this.sessionsId;
    }
}
