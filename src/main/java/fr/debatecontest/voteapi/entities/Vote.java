package fr.debatecontest.voteapi.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import java.util.HashMap;
import java.util.Map;

@Entity
public class Vote {

    @Id
    @GeneratedValue
    private Long id;
    private Long sessionId;
    private Long startTime;
    private InstantState state = InstantState.INCOMING;
    private Long endTime;
    @ElementCollection
    @Cascade(CascadeType.ALL)
    private Map<Long, Long> votes = new HashMap<>();

    public Vote() {
    }

    public Vote(Long id, Long sessionId) {
        this.id = id;
        this.sessionId = sessionId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public InstantState getState() {
        return state;
    }

    public void setState(InstantState state) {
        this.state = state;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Map<Long, Long> getVotes() {
        return votes;
    }

    public void setVotes(Map<Long, Long> votes) {
        this.votes = votes;
    }

}
