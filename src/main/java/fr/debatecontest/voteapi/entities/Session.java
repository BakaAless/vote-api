package fr.debatecontest.voteapi.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Session {

    @Id
    @GeneratedValue
    private Long id;
    private InstantState state = InstantState.INCOMING;
    @ManyToMany(fetch = FetchType.LAZY)
    @Cascade(CascadeType.ALL)
    private List<Candidate> candidates = new ArrayList<>();
    @OneToMany(fetch = FetchType.LAZY)
    @Cascade(CascadeType.ALL)
    private List<Vote> votes = new ArrayList<>();

    public Session() {
    }

    public Session(Long id, List<Candidate> candidates, List<Vote> votes) {
        this.id = id;
        this.candidates = candidates;
        this.votes = votes;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public InstantState getState() {
        return state;
    }

    public void setState(InstantState state) {
        this.state = state;
    }

    public List<Candidate> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }
}
