package fr.debatecontest.voteapi.entities;

public enum InstantState {
    INCOMING,
    RUNNING,
    FINISHED
}
