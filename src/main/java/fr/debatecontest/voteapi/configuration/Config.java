package fr.debatecontest.voteapi.configuration;

import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.yaml.YAMLConfigurationLoader;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.DumperOptions;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Component
public class Config {

    private final File fileConfig;
    private boolean firstConfig;
    private YAMLConfigurationLoader loader;
    private ConfigurationNode config;

    public Config() {
        this(Path.of("config"), "config.yml", Config.class.getClassLoader().getResourceAsStream("config/config.yml"));
    }

    protected Config(final Path dataDirectory, final String fileName) {
        this.fileConfig = new File(dataDirectory.toFile(), fileName);
        this.firstConfig = false;
        try {
            if (!this.fileConfig.exists()) {
                this.fileConfig.getParentFile().mkdirs();
                this.fileConfig.createNewFile();
                this.firstConfig = true;
            }
            this.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected Config(final Path dataDirectory, final String fileName, final String resourcePath) {
        this(dataDirectory, fileName);
        if (this.isFirstConfig()) {
            try {
                final InputStream link = getClass().getResourceAsStream(resourcePath);
                Files.copy(link, Path.of(dataDirectory.toFile().getAbsolutePath() + "/" + fileName), StandardCopyOption.REPLACE_EXISTING);
                this.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    protected Config(final Path dataDirectory, final String fileName, final InputStream stream) {
        this(dataDirectory, fileName);
        if (this.isFirstConfig()) {
            try {
                Files.copy(stream, Path.of(dataDirectory.toFile().getAbsolutePath() + "/" + fileName), StandardCopyOption.REPLACE_EXISTING);
                this.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static Config load(final Path dataDirectory, final String fileName) {
        return new Config(dataDirectory, fileName);
    }

    public static Config load(final Path dataDirectory, final String fileName, final String resourcePath) {
        return new Config(dataDirectory, fileName, resourcePath);
    }

    public static <T> void write(final ConfigurationNode root, final String path, final T value) {
        if (path.contains(".")) {
            final String[] split = path.split("\\.");
            ConfigurationNode node = root;
            for (final String s : split) {
                node = node.getNode(s);
            }
            node.setValue(value);
        } else {
            root.getNode(path).setValue(value);
        }
    }

    public static <T> T getOrWrite(final ConfigurationNode root, final String path, final T default_) {
        if (path.contains(".")) {
            final String[] split = path.split("\\.");
            ConfigurationNode node = root;
            for (final String s : split) {
                node = node.getNode(s);
            }
            if (node.getValue() == null)
                node.setValue(default_);
            return (T) node.getValue();
        } else {
            if (root.getNode(path).getValue() == null)
                root.getNode(path).setValue(default_);
            return (T) root.getNode(path).getValue();
        }
    }

    public <T> void write(final String path, final T value) {
        write(this.config, path, value);
    }

    public void load() throws IOException {
        this.loader = YAMLConfigurationLoader.builder().setFile(this.fileConfig)
                .setFlowStyle(DumperOptions.FlowStyle.BLOCK)
                .build();
        this.config = this.loader.load();
    }

    public <T> T getOrWrite(final String path, final T default_) {
        return getOrWrite(this.config, path, default_);
    }

    public <T> T get(final String path, final T default_) {
        if (path.contains(".")) {
            final String[] split = path.split("\\.");
            ConfigurationNode node = this.config;
            for (final String s : split) {
                node = node.getNode(s);
            }
            if (node.getValue() == null)
                return default_;
            return (T) node.getValue();
        } else {
            if (this.config.getNode(path).getValue() == null)
                return default_;
            return (T) this.config.getNode(path).getValue();
        }
    }

    public List<String> getKeys(final String path) {
        if (path.contains(".")) {
            final String[] split = path.split("\\.");
            ConfigurationNode currentNode = this.config;
            for (final String s : split)
                currentNode = currentNode.getNode(s);
            return currentNode.getChildrenMap().keySet().stream().map(Object::toString).toList();
        } else {
            return this.config.getNode(path).getChildrenMap().keySet().stream().map(Object::toString).toList();
        }
    }

    public void save() throws IOException {
        this.loader.save(this.config);
    }

    public boolean isFirstConfig() {
        return this.firstConfig;
    }

    public Path getPath() {
        return this.fileConfig.getAbsoluteFile().toPath();
    }
}
